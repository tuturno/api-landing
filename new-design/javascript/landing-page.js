$(document).ready(function() {

  var client = new ZeroClipboard($('.code-copy-button'));

  client.on( "ready", function( readyEvent ) {
    client.on( "copy", function (event) {
      var clipboard = event.clipboardData;
      clipboard.setData( "text/plain", event.target.parentElement.children[0].textContent);
    });

    client.on( "aftercopy", function( event ) {
      event.target.textContent = '✓';
      setTimeout(function() {
        event.target.textContent = 'Copy';
      }, 1000)
    });
  });

  var codeBodies = $('.code-body');

  for(var i = 0; i < codeBodies.length; i++) {
    $(codeBodies[i]).hover(function() {
      $(this.children[1]).css('opacity', 1);
    }, function() {
      $(this.children[1]).css('opacity', 0);
    })
  }
});
