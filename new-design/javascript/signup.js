$(document).ready(function () {
  function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  function styleInputOnValidity(inputType) {
    switch(inputType) {
      case 'email':
        if (validateEmail($('#signup-email').val())) {
          displayValid(inputType);
        } else {
          displayInvalid(inputType);
        }
        break;
      case 'password':
        if ($('#signup-password').val().length >= 8) {
          displayValid(inputType);
        } else {
          displayInvalid(inputType);
        }
        break;
      default:
        break;
    }
  }

  function displayValid(inputType) {
    $('#signup-' + inputType).css('border-color', '#69E5B2');
    $('.' + inputType + '-section img.positive-tick').css('height', '26px');
    $('.' + inputType + '-section img.sad-face').css('height', '0');
  }

  function displayInvalid(inputType) {
    $('#signup-' + inputType).css('border-color', 'red');
    $('.' + inputType + '-section img.positive-tick').css('height', '0');
    $('.' + inputType + '-section img.sad-face').css('height', '26px');
  }

  function movePasswordValidationImage(move) {
    if (move) {
      $('.password-section img').css('top', '21px');
    } else {
      $('.password-section img').css('top', '7px');
    }
  }

  // mailcheck for the email input field
  var domains = ['hotmail.com', 'gmail.com', 'aol.com'];
  var topLevelDomains = ["com", "net", "org"];

  $('#signup-email').on('blur', function(event) {
    $(this).mailcheck({
      domains: domains,                       // optional
      topLevelDomains: topLevelDomains,       // optional
      suggested: function(element, suggestion) {
        movePasswordValidationImage(true);
        $('.suggestion-link').html(suggestion.full);
        $('.suggestion').css('visibility', 'visible');
        $('.suggestion').css('height', '20px');
      },
      empty: function(element) {
        styleInputOnValidity('email');
      }
    });
  });

  $('#signup-password').on('blur', function(event) {
    styleInputOnValidity('password');
  });

  window.addEventListener('resize', function(event) {
    var contentBoxOffset = $('#content-box').offset().top;
    if (contentBoxOffset < 60) {
      $('.navbar-mini').css('display', 'none');
    } else {
      $('.navbar-mini').css('display', 'block');
    }
  });

  $('.suggestion-link').on('click', function() {
    $('#signup-email').val($('.suggestion-link').text());
    $('.suggestion').css('visibility', 'hidden');
    $('.suggestion').css('height', 0);
    styleInputOnValidity('email');
    movePasswordValidationImage(false);
  });
});