// Controls how the navbar swaps to fixed navbar on scroll position change
$(document).ready(function () {
  var navbarChangeoverPoint = 400;
  var navBar = $('.navbar');
  var signUpButton = $('button.signup-button');
  var signUpLink = $('.navbar-nav .signup-link');

  $(window).scroll(function() {
    var scrollPosition = $(window).scrollTop();

    if (scrollPosition >= navbarChangeoverPoint - 50) {
      navBar.css('top', "-100px");
    }
    else {
      navBar.css('top', "0");
    }

    if (scrollPosition >= navbarChangeoverPoint + 50 && !navBar.hasClass('navbar-fixed-top')) {
      navBar.toggleClass("navbar-sticky navbar-fixed-top");
      $('#nav-ul').toggleClass("fixed-top-padding");
      signUpButton.toggleClass('hidden');
      signUpLink.toggleClass('hidden');
    }
    else if (scrollPosition < navbarChangeoverPoint + 50  && navBar.hasClass('navbar-fixed-top')) {
      navBar.toggleClass('navbar-sticky navbar-fixed-top');
      $('#nav-ul').toggleClass("fixed-top-padding");
      signUpButton.toggleClass('hidden');
      signUpLink.toggleClass('hidden');
    }
  });
});