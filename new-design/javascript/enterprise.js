$(document).ready(function(){
  $('.carousel-pane').slick({
    autoplay: true,
    autoplaySpeed: 9000,
    speed: 400,
    onBeforeChange: renameArrowText,
    prevArrow: '<div class="carousel-controller prev-controller"><i class="ionicons ion-ios7-arrow-thin-left arrow-control"></i><span class="previous-carousel-item arrow-text">PREVIOUS</span></div>',
    nextArrow: '<div class="carousel-controller next-controller"><i class="ionicons ion-ios7-arrow-thin-right arrow-control"></i><span class="next-carousel-item arrow-text">NEXT</span></div>',
    appendArrows: $('.arrows-container')
  });

  // ------------------------------------------------------- //
  // Code to add names of next carousel items next to the arrows
  var items = $('.carousel-item');

  renameArrowText($('.carousel-pane').getSlick(), 0, 0);

  function renameArrowText(slick, currentIndex, targetIndex) {
    for (var i = 0; i < items.length; i++) {
      if ($(items[i]).attr('index') == (((targetIndex + 1) % 5) + 5) % 5) {
        $('.next-carousel-item').text($(items[i]).data('name'));
      } else if ($(items[i]).attr('index') == (((targetIndex - 1) % 5) + 5) % 5) {
        $('.previous-carousel-item').text($(items[i]).data('name'));
      }
    }
  }
  // ------------------------------------------------------- //

  $('.contact-button').click(function() {
    window.setTimeout( function() {
      $('.contact-button').blur();
      showSales();
      $('#sales-name').focus();
    }, 500)
    $('html, body').animate({
      scrollTop: $(".contact-section").offset().top - 100
    }, 1000);
  });
});