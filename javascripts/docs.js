$( function() {
  var language_names = {
    'js': 'JavaScript',
    'node': "Node.js",
    'wordpress': "PHP/WordPress",
    'drupal': "PHP/Drupal",
    'php': "PHP",
    'python': "Python",
    'py-gae': 'Python/GAE',
    'py-flask': 'Python (Flask)',
    'rb': 'Ruby',
    'rails': 'Rails',
    'html': 'HTML',
    'cs-sl': '.NET &amp Silverlight',
    'cs': '.NET',    
    'py': 'Python',
    'sl': 'Silverlight',
    'as': 'ActionScript',
    'ios': 'iOS',
    'cs-mvc': 'ASP.NET MVC',
    'go': 'Go',
    'java': 'Java'
  };
  var language_classes = {
    'rails': "rb",
    'wordpress': "php",
    'drupal': "php",
    "node": "js",
    'cs-mvc': "cs",
    'py-gae': "python"
  };

  $('div.code').each(function() {
    var div_code = $(this);
    var counter = 0;
    var ul = $(document.createElement('ul'));
    div_code.find('code').each(function() {
      var codeEl = $(this);
      var lang = codeEl.attr('data-lang');
      if (language_classes[lang]) {
        codeEl.addClass(language_classes[lang])
      } else {
        codeEl.addClass(lang)
      }
      codeEl.html($.trim(codeEl.html())); // don't tell micheil
      codeEl.wrap("<pre />");
      
      var linkEl = $('<a href="#" rel="'+lang+'">'+language_names[lang]+'</a>');
      var listItemEl = $('<li></li>');
      listItemEl.append(linkEl);
      ul.append(listItemEl);
      
      linkEl.click(function(e) {
        e.preventDefault();
        
        if ($(this).hasClass('active')) return;
        
        div_code.find('code').parent().hide();
        div_code.find('code[data-lang='+$(this).attr('rel')+']').parent().show();
        div_code.find('a').removeClass('active');
        $(this).addClass('active');
      });
      
      // This should be fixed to store the lang pref in a cookie and show that by default on page load
      div_code.prepend(ul)
      div_code.find('pre:not(:first)').hide();
      div_code.find('a:first').addClass('active');
    });
  });
  
  function showCodeFor(lang) {
    $("a[rel='" + lang + "']").click();
  }
  
  function getHashValues() {
    var hash = document.location.hash;
    hash = hash.replace("#", "");
    var parts = hash.split('&');
    var vals = {};
    $.each(parts, function(index, value) {
      var nameValue = value.split('=');
      vals[nameValue[0]] = nameValue[1];
    });
    return vals;
  }
  
  var hashVals = getHashValues();
  var lang = hashVals['lang'];
  if(lang) {
    showCodeFor(lang)
  }

  // New code / docs functionality
  var allGroupEls = $( '*[data-group-id]' );

  // group examples together by data-group-id
  var groups = {};
  allGroupEls.each( function() {
    var el = $( this );
    var groupId = el.data( 'group-id' );
    if( groups[ groupId ] === undefined ) {
      groups[ groupId ] = [];
    }
    groups[ groupId ].push( el );
  } );

  // console.log( 'found groups' );
  // console.log( groups );

  // For each group create a tab set for the group functionality.
  $.each( groups, function( groupId, groupEls ) {
    // console.log( 'creating tabset for: ' + groupId + ' with ' + groupEls.length + ' elements' );

    var tabsetUl = $( '<ul class="nav nav-tabs"></ul>' );
    var tabsetContent = $( '<div class="tab-content"></div>' );

    var firstEl = groupEls[0];
    firstEl.before( tabsetUl );
    firstEl.before( tabsetContent );

    createTabs( groupId, groupEls, tabsetUl, tabsetContent );
  } );

  function createTabs( groupId, groupEls, tabsetUl, tabsetContent ) {
    // Create a tab for each language (data-lang)
    var langTabContentEls = {};
    $.each( groupEls,  function( i, el ) {
      el = $( el );
      var langAbbr = el.data( 'lang' );
      var langName = language_names[ langAbbr ];
      var tabId = groupId + '_' + langAbbr;
      var tabContent = langTabContentEls[ langAbbr ];
      if( tabContent === undefined ) {
        var tabHead =  $( '<li class="'+ ( i === 0? 'active' : '' ) + '">' +
                            '<a href="#' + tabId + '" data-toggle="tab" data-lang="' + langAbbr + '">' +
                              langName + 
                            '</a>' +
                          '</li>' );
        tabsetUl.append( tabHead );

        tabContent = $( '<div class="tab-pane fade'+ ( i === 0? ' active in' : '' ) + '" id="' + tabId + '"></div>' );
        tabsetContent.append( tabContent );

        langTabContentEls[ langAbbr ] = tabContent;
      }

      // Append the elements (pre for code and article for docs)
      // to the appropriate tab content el
      var clone = el.clone();
      tabContent.append( clone );
      el.remove();
    } );
  }

  // old code blocks
  $('div.code pre code[data-lang]').addClass( 'prettyprint' );

  // new code blocks
  $('pre[data-lang] code').addClass( 'prettyprint' );

  prettyPrint();

  var langHashRegexp = /\/lang=([\w-]+)/;

  $('.nav-tabs a[data-toggle="tab"]').click(function (e) {

    var currentHash = document.location.hash;
    var langSelected = $( this ).data( 'lang' );
    var langHash = '/lang=' + langSelected;
    if( langHashRegexp.test( currentHash ) ) {
      document.location.hash = document.location.hash.replace( langHashRegexp, langHash )
    }
    else {
      document.location.hash += langHash;
    }

  } );

  var langMatch = langHashRegexp.exec( document.location.hash );
  if( langMatch && language_names[ langMatch[1] ] ) {
    $( '.nav-tabs a[data-lang="' + langMatch[1] + '"]' ).tab( 'show' );
  }
  
} );
