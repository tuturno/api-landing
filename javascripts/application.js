$(function() {
  if(document.location.hash) {
    setTimeout(adjustForTopBar, 0);
  }
  
  $("a").click(function() {
    var el = $(this);
    if(el.attr('href').indexOf('#') === 0) {
      setTimeout(adjustForTopBar, 0);
    }
  })
  
  function adjustForTopBar() {
    var body = $('body');
    var st = body.scrollTop();
    var fixedSize = $('.topbar .fill').height();
    body.scrollTop(st-fixedSize); // adjust for the fixed height top bar
  }
});