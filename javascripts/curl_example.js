function curlExample(triggerSelector, options) {
  options = options || {};
  var settings = {
    sendToSignup: false,
    alertText: null,
    closeAfterEvent: true
  };
  $.extend(settings, options);

  var random_key = (function(){
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
     var rnum = Math.floor(Math.random() * chars.length);
     randomstring += chars.substring(rnum,rnum+1);
    }
    return randomstring;
  })();

  var pusher = new Pusher('765ec374ae0a69f4ce44');
  var channel = pusher.subscribe(random_key);
  channel.bind('my_event', function(msg) {
    if(settings.alertText) {
      alert(settings.alertText);
    }
    if(settings.sendToSignup) {
      window.location = "http://pusher.com/pricing";
      window.analytics.track('Did curl example');
    }
    if(settings.closeAfterEvent) {
      $.fancybox.close();
    }
  });

  $(document).ready(function(){
    var trigger = $(triggerSelector);
    if(settings.sendToSignup) {
      trigger.click(function() { // only track if expecting to sign up
        window.analytics.track('Opened curl example');
      });
    }
    trigger.attr('href', '/curl_command?channel='+random_key);
    trigger.fancybox({
        width: 596,
        height: 200,
        autoDimensions: false
    });
  });

}
