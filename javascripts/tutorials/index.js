$(document).ready(function(){
  $('.tutorial-block-panel').hover(function(){
    $(this).find('.overlay').fadeIn();
  }, function(){
    $(this).find('.overlay').fadeOut("fast");
  }); 

  tutorials.forEach(function(tutorial){
    var link = document.getElementById(tutorial.id);
    analytics.trackLink(link, 'Clicked tutorial', tutorial);
  });

  var getStarted = document.getElementById('get-started');
  analytics.trackLink(getStarted, 'Clicked Getting Started Guide');

});